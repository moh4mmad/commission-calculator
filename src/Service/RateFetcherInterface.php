<?php

namespace CommissionCalculator\Service;

interface RateFetcherInterface
{
    public function fetchRate(string $currency): float;
}
