<?php

namespace CommissionCalculator\Service;

use GuzzleHttp\Client;

class RateFetcher implements RateFetcherInterface
{
    private $currencyApi;
    private $httpClient;
    private $rates;

    public function __construct()
    {
        $this->currencyApi = "https://developers.paysera.com/tasks/api/currency-exchange-rates";
        $this->httpClient = new Client();
        $this->rates = null;
    }

    public function fetchRate(string $currency): float
    {
        if ($this->rates === null) {
            $this->fetchRatesData();
        }

        if (!isset($this->rates[$currency])) {
            throw new \RuntimeException("Rate for currency $currency not found.");
        }

        return $this->rates[$currency];
    }

    private function fetchRatesData(): void
    {
        try {
            $response = $this->httpClient->request('GET', $this->currencyApi);
            $json = $response->getBody()->getContents();
            $data = json_decode($json, true);

            if (!isset($data['rates'])) {
                throw new \RuntimeException("Invalid rates data received from the API.");
            }

            $this->rates = $data['rates'];
        } catch (\Exception $e) {
            throw new \RuntimeException("Failed to fetch rates data from the API.");
        }
    }
}
