<?php

namespace CommissionCalculator\Data;

class Constants
{
    public const OPERATION_TYPE_DEPOSIT = 'deposit';
    public const OPERATION_TYPE_WITHDRAW = 'withdraw';

    public const USER_TYPE_PRIVATE = 'private';
    public const USER_TYPE_BUSINESS = 'business';

    public const DEPOSITE_RATE = 0.03;
    public const COMMISSION_RATE_PRIVATE = 0.3;
    public const COMMISSION_RATE_BUSINESS = 0.5;
    public const THRESHOLD_AMOUNT = 1000.00;
    public const DECIMAL_PLACES = 2;
}
