<?php

namespace CommissionCalculator\Calculator;

use CommissionCalculator\Model\Operation;
use CommissionCalculator\Data\Constants;
use CommissionCalculator\Calculator\Interfaces\OperationProcessorInterface;
use CommissionCalculator\Calculator\DepositCommissionCalculator;
use CommissionCalculator\Calculator\WithdrawCommissionCalculator;
use CommissionCalculator\Service\RateFetcher;

class OperationProcessor implements OperationProcessorInterface
{
    private $rateFetcher;
    private $timeline;
    private $sum;
    private $weekMatched;
    private $operationCount;
    private $thresholdCrossed;
    private $depositCommissionCalculator;
    private $withdrawCommissionCalculator;


    public function __construct(
        RateFetcher $rateFetcher,
        DepositCommissionCalculator $depositCommissionCalculator,
        WithdrawCommissionCalculator $withdrawCommissionCalculator
    ) {
        $this->rateFetcher = $rateFetcher;
        $this->depositCommissionCalculator = $depositCommissionCalculator;
        $this->withdrawCommissionCalculator = $withdrawCommissionCalculator;
        $this->timeline = [];
        $this->sum = 0;
        $this->weekMatched = false;
        $this->operationCount = 0;
        $this->thresholdCrossed = false;
    }

    public function process(Operation $operation): float
    {
        if ($operation->getOperationType() === Constants::OPERATION_TYPE_DEPOSIT) {
            return $this->depositCommissionCalculator->calculateCommission($operation);
        }

        if ($operation->getOperationType() === Constants::OPERATION_TYPE_WITHDRAW) {
            return $this->withdrawCommissionCalculator->calculateCommission($operation);
        }

        throw new \Exception('Invalid operation type: ' . $operation->getOperationType());
    }
}
