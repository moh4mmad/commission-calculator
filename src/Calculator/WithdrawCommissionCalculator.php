<?php

namespace CommissionCalculator\Calculator;

use CommissionCalculator\Model\Operation;
use CommissionCalculator\Data\Constants;
use CommissionCalculator\Calculator\Interfaces\CommissionCalculatorInterface;
use CommissionCalculator\Calculator\PrivateWithdrawCommissionCalculator;
use CommissionCalculator\Calculator\BusinessWithdrawCommissionCalculator;
use CommissionCalculator\Service\RateFetcher;

class WithdrawCommissionCalculator implements CommissionCalculatorInterface
{
    private $privateWithdrawCommissionCalculator;
    private $businessWithdrawCommissionCalculator;

    public function __construct(
        PrivateWithdrawCommissionCalculator $privateWithdrawCommissionCalculator,
        BusinessWithdrawCommissionCalculator $businessWithdrawCommissionCalculator
    ) {
        $this->privateWithdrawCommissionCalculator = $privateWithdrawCommissionCalculator;
        $this->businessWithdrawCommissionCalculator = $businessWithdrawCommissionCalculator;
    }

    public function calculateCommission(Operation $operation): float
    {
        $commission = 0;
        if ($operation->getUserType() === Constants::USER_TYPE_PRIVATE) {
            $commission = $this->privateWithdrawCommissionCalculator->calculateCommission($operation);
        } elseif ($operation->getUserType() === Constants::USER_TYPE_BUSINESS) {
            $commission = $this->businessWithdrawCommissionCalculator->calculateCommission($operation);
        }
        return $commission;
    }
}
