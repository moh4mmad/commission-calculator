<?php

namespace CommissionCalculator\Calculator\Interfaces;

use CommissionCalculator\Model\Operation;

interface OperationProcessorInterface
{
    public function process(Operation $operation): float;
}
