<?php

namespace CommissionCalculator\Calculator\Interfaces;

use CommissionCalculator\Model\Operation;

interface CommissionCalculatorInterface
{
    public function calculateCommission(Operation $data): float;
}
