<?php

namespace CommissionCalculator\Calculator;

use CommissionCalculator\Calculator\Interfaces\CommissionCalculatorInterface;
use CommissionCalculator\Data\Constants;
use CommissionCalculator\Model\Operation;
use CommissionCalculator\Service\RateFetcher;

class PrivateWithdrawCommissionCalculator implements CommissionCalculatorInterface
{

    private $timeline;
    private $sum;
    private $weekMatched;
    private $operationCount;
    private $thresholdCrossed;
    private $rateFetcher;


    public function __construct(RateFetcher $rateFetcher)
    {
        $this->rateFetcher = $rateFetcher;
        $this->timeline = [];
        $this->sum = 0;
        $this->weekMatched = false;
        $this->operationCount = 0;
        $this->thresholdCrossed = false;
    }

    public function calculateCommission(Operation $operation): float
    {
        $userId = $operation->getUserId();
        $currentDate = strtotime($operation->getDate());

        if (!isset($this->timeline[$userId])) {
            $this->timeline[$userId] = $currentDate;
            $this->operationCount = 1;
            $this->weekMatched = false;
            $this->sum = 0;
            $this->thresholdCrossed = false;
        } else {
            $storedWeek = date('W', $this->timeline[$userId]);
            $storedYear = date('Y', $this->timeline[$userId]);
            $currentWeek = date('W', $currentDate);
            $currentYear = date('Y', $currentDate);

            if (($storedYear < $currentYear && ($currentYear - $storedYear) == 1 && $storedWeek == $currentWeek)
                || ($storedYear == $currentYear && $storedWeek == $currentWeek)
            ) {
                $this->weekMatched = true;
                ++$this->operationCount;
            } else {
                $this->weekMatched = false;
                $this->timeline[$userId] = $currentDate;
                $this->sum = 0;
                $this->operationCount = 0;
                $this->thresholdCrossed = false;
            }
        }

        if ($this->operationCount > 3) {
            $commission = $operation->getAmount() * (Constants::COMMISSION_RATE_PRIVATE / 100);
        } else {
            $commission = $this->calculatePrivateWithdrawCommissionSecondRule($operation);
        }

        return $commission;
    }

    private function calculatePrivateWithdrawCommissionSecondRule(Operation $operation): float
    {
        $currency = $operation->getCurrency();
        $amount = $operation->getAmount();

        if ($currency !== 'EUR') {
            $rate = $this->rateFetcher->fetchRate($currency);
            $baseAmount = ($amount / $rate) + $this->sum;
        } else {
            $baseAmount = $amount + $this->sum;
        }

        if ($this->thresholdCrossed) {
            $commission = $baseAmount * (Constants::COMMISSION_RATE_PRIVATE / 100);
        } elseif ($baseAmount > Constants::THRESHOLD_AMOUNT) {
            $exceededAmount = $baseAmount - Constants::THRESHOLD_AMOUNT;
            $commission = $exceededAmount * (Constants::COMMISSION_RATE_PRIVATE / 100);

            if ($currency !== 'EUR') {
                $commission = $commission * $rate;
            }

            $this->thresholdCrossed = true;
            $this->sum = 0;
        } else {
            if (Constants::THRESHOLD_AMOUNT > $baseAmount) {
                $this->sum += $baseAmount;
            }
            $commission = 0;
        }

        return $commission;
    }
}
