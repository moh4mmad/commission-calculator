<?php

namespace CommissionCalculator\Calculator;

use CommissionCalculator\Calculator\Interfaces\CommissionCalculatorInterface;
use CommissionCalculator\Data\Constants;
use CommissionCalculator\Model\Operation;

class DepositCommissionCalculator
{
    public static function calculateCommission(Operation $operation): float
    {
        $commission = $operation->getAmount() * (Constants::DEPOSITE_RATE / 100);
        return $commission;
    }
}
