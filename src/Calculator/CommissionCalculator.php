<?php

namespace CommissionCalculator\Calculator;

use CommissionCalculator\Calculator\OperationProcessor;
use CommissionCalculator\Model\Operation;
use CommissionCalculator\Data\Constants;

class CommissionCalculator
{
    private $operationProcessor;

    public function __construct(OperationProcessor $operationProcessor)
    {
        $this->operationProcessor = $operationProcessor;
    }

    public function calculateCommission(Operation $data)
    {
        $result = $this->operationProcessor->process($data);
        return number_format($result, Constants::DECIMAL_PLACES, '.', '');
    }
}
