<?php

namespace CommissionCalculator\Calculator;

use CommissionCalculator\Calculator\Interfaces\CommissionCalculatorInterface;
use CommissionCalculator\Data\Constants;
use CommissionCalculator\Model\Operation;

class BusinessWithdrawCommissionCalculator implements CommissionCalculatorInterface
{
    public function calculateCommission(Operation $operation): float
    {
        $commission = $operation->getAmount() * (Constants::COMMISSION_RATE_BUSINESS / 100);
        return $commission;
    }
}
