<?php

namespace CommissionCalculator\Utils;

use CommissionCalculator\Model\Operation;

class CsvParser
{

    private $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function parse(): array
    {
        $lines = file($this->filePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $operations = [];

        foreach ($lines as $line) {
            $operation = str_getcsv($line);
            $operations[] = new Operation(
                $operation[0],
                (int) $operation[1],
                $operation[2],
                $operation[3],
                (float) $operation[4],
                $operation[5]
            );
        }

        return $operations;
    }
}
