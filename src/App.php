<?php

namespace CommissionCalculator;

use CommissionCalculator\Calculator\CommissionCalculator;
use CommissionCalculator\Service\RateFetcher;
use CommissionCalculator\Utils\CsvParser;
use CommissionCalculator\Calculator\OperationProcessor;
use CommissionCalculator\Calculator\BusinessWithdrawCommissionCalculator;
use CommissionCalculator\Calculator\PrivateWithdrawCommissionCalculator;
use CommissionCalculator\Calculator\WithdrawCommissionCalculator;
use CommissionCalculator\Calculator\DepositCommissionCalculator;

class App
{
    private $rateFetcher;
    private $depositCommissionCalculator;
    private $privateWithdrawCommissionCalculator;
    private $businessWithdrawCommissionCalculator;
    private $withdrawCommissionCalculator;
    private $operationProcessor;
    private $commissionCalculator;


    public function __construct()
    {
        $this->rateFetcher = new RateFetcher();
        $this->depositCommissionCalculator = new DepositCommissionCalculator();
        $this->privateWithdrawCommissionCalculator = new PrivateWithdrawCommissionCalculator($this->rateFetcher);
        $this->businessWithdrawCommissionCalculator = new BusinessWithdrawCommissionCalculator();
        $this->withdrawCommissionCalculator = new WithdrawCommissionCalculator(
            $this->privateWithdrawCommissionCalculator,
            $this->businessWithdrawCommissionCalculator
        );
        $this->operationProcessor = new OperationProcessor(
            $this->rateFetcher,
            $this->depositCommissionCalculator,
            $this->withdrawCommissionCalculator
        );
        $this->commissionCalculator = new CommissionCalculator($this->operationProcessor);
    }

    public function run($csvFile)
    {
        $csvParser = new CsvParser($csvFile);
        $operations = $csvParser->parse();
        $results = [];
        foreach ($operations as $operation) {
            $commission = $this->commissionCalculator->calculateCommission($operation);
            $results[] = $commission;
        }
        return $results;
    }
}
