<?php

use PHPUnit\Framework\TestCase;
use CommissionCalculator\Calculator\CommissionCalculator;
use CommissionCalculator\Calculator\DepositCommissionCalculator;
use CommissionCalculator\Calculator\PrivateWithdrawCommissionCalculator;
use CommissionCalculator\Calculator\BusinessWithdrawCommissionCalculator;
use CommissionCalculator\Calculator\WithdrawCommissionCalculator;
use CommissionCalculator\Calculator\OperationProcessor;
use CommissionCalculator\Model\Operation;
use CommissionCalculator\Service\RateFetcher;

class FinalCommissionCalculatorTest extends TestCase
{
    private $commissionCalculator;

    protected function setUp(): void
    {
        $rateFetcher = new RateFetcher();
        $depositCommissionCalculator = new DepositCommissionCalculator();
        $privateWithdrawCommissionCalculator = new PrivateWithdrawCommissionCalculator($rateFetcher);
        $businessWithdrawCommissionCalculator = new BusinessWithdrawCommissionCalculator();
        $withdrawCommissionCalculator = new WithdrawCommissionCalculator(
            $privateWithdrawCommissionCalculator,
            $businessWithdrawCommissionCalculator
        );
        $operationProcessor = new OperationProcessor($rateFetcher, $depositCommissionCalculator, $withdrawCommissionCalculator);
        $this->commissionCalculator = new CommissionCalculator($operationProcessor);
    }

    public function testCalculateCommission(): void
    {
        $operations = [
            ['2014-12-31', 4, 'private', 'withdraw', 1200.00, 'EUR'],
            ['2015-01-01', 4, 'private', 'withdraw', 1000.00, 'EUR'],
            ['2016-01-05', 4, 'private', 'withdraw', 1000.00, 'EUR'],
            ['2016-01-05', 1, 'private', 'deposit', 200.00, 'EUR'],
            ['2016-01-06', 2, 'business', 'withdraw', 300.00, 'EUR'],
            ['2016-01-06', 1, 'private', 'withdraw', 30000, 'JPY'],
            ['2016-01-07', 1, 'private', 'withdraw', 1000.00, 'EUR'],
            ['2016-01-07', 1, 'private', 'withdraw', 100.00, 'USD'],
            ['2016-01-10', 1, 'private', 'withdraw', 100.00, 'EUR'],
            ['2016-01-10', 2, 'business', 'deposit', 10000.00, 'EUR'],
            ['2016-01-10', 3, 'private', 'withdraw', 1000.00, 'EUR'],
            ['2016-02-15', 1, 'private', 'withdraw', 300.00, 'EUR'],
            ['2016-02-19', 5, 'private', 'withdraw', 3000000, 'JPY'],
        ];

        $expectedCommissions = [
            '0.60',
            '3.00',
            '0.00',
            '0.06',
            '1.50',
            '0.00',
            '0.69',
            '0.27',
            '0.30',
            '3.00',
            '0.00',
            '0.00',
            '8607.39'
        ];

        $actualCommissions = [];
        foreach ($operations as $operation) {
            $data = new Operation(
                $operation[0],
                (int) $operation[1],
                $operation[2],
                $operation[3],
                (float) $operation[4],
                $operation[5]
            );
            $commission = $this->commissionCalculator->calculateCommission($data);
            $actualCommissions[] = $commission;
        }

        $this->assertEquals($expectedCommissions, $actualCommissions);
    }
}
