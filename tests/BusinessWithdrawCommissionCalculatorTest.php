<?php

use CommissionCalculator\Calculator\BusinessWithdrawCommissionCalculator;
use CommissionCalculator\Model\Operation;
use PHPUnit\Framework\TestCase;

class BusinessWithdrawCommissionCalculatorTest extends TestCase
{
    public function testCalculateCommission(): void
    {
        $operation = new Operation('2016-01-06', 2, 'business', 'withdraw', 300.00, 'EUR');
        $calculator = new BusinessWithdrawCommissionCalculator();

        $expectedCommission = 1.5;
        $commission = $calculator->calculateCommission($operation);

        $this->assertEquals($expectedCommission, $commission);
    }
}
