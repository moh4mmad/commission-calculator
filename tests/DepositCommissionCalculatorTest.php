<?php

use CommissionCalculator\Calculator\DepositCommissionCalculator;
use CommissionCalculator\Model\Operation;
use PHPUnit\Framework\TestCase;

class DepositCommissionCalculatorTest extends TestCase
{
    public function testDepositCalculateCommission(): void
    {
        $operation = new Operation('2016-01-05', 1, 'private', 'deposit', 200.00, 'EUR');
        $calculator = new DepositCommissionCalculator();

        $expectedCommission = 0.06;
        $commission = $calculator->calculateCommission($operation);

        $this->assertEquals($expectedCommission, $commission);
    }
}
