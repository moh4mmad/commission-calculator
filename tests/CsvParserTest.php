<?php

use CommissionCalculator\Model\Operation;
use CommissionCalculator\Utils\CsvParser;
use PHPUnit\Framework\TestCase;

class CsvParserTest extends TestCase
{

    public function testParse(): void
    {
        $csvParser = new CsvParser('./input.csv');
        $operations = $csvParser->parse();

        $operationsData = [
            ['2014-12-31', 4, 'private', 'withdraw', 1200.00, 'EUR'],
            ['2015-01-01', 4, 'private', 'withdraw', 1000.00, 'EUR'],
            ['2016-01-05', 4, 'private', 'withdraw', 1000.00, 'EUR'],
            ['2016-01-05', 1, 'private', 'deposit', 200.00, 'EUR'],
            ['2016-01-06', 2, 'business', 'withdraw', 300.00, 'EUR'],
            ['2016-01-06', 1, 'private', 'withdraw', 30000, 'JPY'],
            ['2016-01-07', 1, 'private', 'withdraw', 1000.00, 'EUR'],
            ['2016-01-07', 1, 'private', 'withdraw', 100.00, 'USD'],
            ['2016-01-10', 1, 'private', 'withdraw', 100.00, 'EUR'],
            ['2016-01-10', 2, 'business', 'deposit', 10000.00, 'EUR'],
            ['2016-01-10', 3, 'private', 'withdraw', 1000.00, 'EUR'],
            ['2016-02-15', 1, 'private', 'withdraw', 300.00, 'EUR'],
            ['2016-02-19', 5, 'private', 'withdraw', 3000000, 'JPY'],
        ];

        $expectedOperations = [];
        foreach ($operationsData as $operation) {
            $expectedOperations[] = new Operation(
                $operation[0],
                (int) $operation[1],
                $operation[2],
                $operation[3],
                (float) $operation[4],
                $operation[5]
            );
        }
        $this->assertEquals($expectedOperations, $operations);
    }
}
