<?php

require_once __DIR__ . '/vendor/autoload.php';

use CommissionCalculator\App;

function runCommissionCalculator($csvFile)
{
    $app = new App();
    $results = $app->run($csvFile);
    foreach ($results as $result) {
        echo $result . PHP_EOL;
    }
}

//$start = microtime(true);
if (isset($argv[1])) {
    $csvFile = $argv[1];
    runCommissionCalculator($csvFile);
} else {
    echo "Please provide the CSV file name as a command-line argument. Example: php script.php input.csv" . PHP_EOL;
    exit(1);
}
//$end = microtime(true);
//echo 'Response time: ' . ($end - $start) . PHP_EOL;
